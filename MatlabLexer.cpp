//============================================================================
// Name        : MatlabLexer.cpp  (applicable to MatLab / GNU Octave)
// Author      : Rene Haberland
// License     : CC BY-NC  https://creativecommons.org/licenses/by-nd/4.0/
// Description : One-Pass Lexer for a multi-dimensional lexical analysis
// Date        : 2016/2020
//============================================================================
#include <iostream>
#include <stdio.h>
#include <map>
#include <list>
#include <assert.h>

using namespace std;

#define FUNCTION 4
#define LT 29
#define WHILE 6
#define CASE 11
#define FOR 5
#define COMPLEX 64
#define EQUALS 39
#define NOT 35
#define AND 31
#define ID 66
#define DOTTRANSPOSE 24
#define IF 7
#define ANDAND 33
#define LBRACKET 40
#define TRANSPOSE 45
#define LEFTDIVISION 20
#define COMMA 38
#define RSBRACKET 43
#define RIGHTDIVISION 18
#define ARGUMENT 49
#define PLUS 14
#define COLONARGUMENT 53
#define EQ 25
#define RBRACKET 41
#define DOT 44
#define DOTLEFTDIVISION 21
#define LINEBREAK 67
#define OTHERWISE 12
#define NE 30
#define GE 26
#define OROR 34
#define SWITCH 10
#define ELSE 8
#define POWER 22
#define DOTMULTIPLICATION 17
#define DOTRIGHTDIVISION 19
#define SEMICOLON 37
#define MINUS 15
#define ELSEIF 9
#define THREE_DOTS 70
#define LSBRACKET 42
#define COLON 36
#define REAL 76
#define WS 69
#define OR 32
#define GT 27
#define MULTIPLICATION 16
#define DOTPOWER 23
#define END 13
#define LE 28
#define STRING 46
#define ERROR 999

static const int _keywords[] = { FUNCTION, FOR, WHILE, IF, ELSE, ELSEIF, SWITCH, CASE, OTHERWISE, END };
list<int> keywords;

typedef struct {
	string key;
	int value;
} t_keywords_dictionary;

typedef struct {
	int key;
	string value;
} t_keywords_naming_dictionary;

static const t_keywords_dictionary _keywords_dictionary[] = {{"function",FUNCTION}, {"for", FOR}, {"while", WHILE}, {"if", IF}, {"else",ELSE}, {"elseif",ELSEIF}, {"switch",SWITCH}, {"case",CASE}, {"otherwise",OTHERWISE}, {"end",END}};
map<string,int> keywords_dictionary;

static const t_keywords_naming_dictionary _keywords_naming_dictionary[] = {{FUNCTION,"function"},{LT,"LT"},{WHILE,"WHILE"},{CASE,"CASE"},{FOR,"FOR"},{COMPLEX,"COMPLEX"},{EQUALS,"EQUALS"},{NOT,"NOT"},{AND,"AND"},{ID,"ID"},{DOTTRANSPOSE,"DOTTRANSPOSE"},{IF,"IF"},{ANDAND,"ANDAND"},{LBRACKET,"LBRACKET"},{TRANSPOSE,"TRANSPOSE"},{LEFTDIVISION,"LEFTDIVISION"},{COMMA,"COMMA"},{RSBRACKET,"RSBRACKET"},{RIGHTDIVISION,"RIGHTDIVISION"},{ARGUMENT,"ARGUMENT"},{PLUS,"PLUS"},{COLONARGUMENT,"COLONARGUMENT"},{EQ,"EQ"},{RBRACKET,"RBRACKET"},{DOT,"DOT"},{DOTLEFTDIVISION,"DOTLEFTDIVISION"},{LINEBREAK,"LINEBREAK"},{OTHERWISE,"OTHERWISE"},{NE,"NE"},{GE,"GE"},{OROR,"OROR"},{SWITCH,"SWITCH"},{ELSE,"ELSE"},{POWER,"POWER"},{DOTMULTIPLICATION,"DOTMULTIPLICATION"},{DOTRIGHTDIVISION,"DOTRIGHTDIVISION"},{SEMICOLON,"SEMICOLON"},{MINUS,"MINUS"},{ELSEIF,"ELSEIF"},{THREE_DOTS,"THREE_DOTS"},{LSBRACKET,"LSBRACKET"},{COLON,"COLON"},{REAL,"REAL"},{WS,"WS"},{OR,"OR"},{GT,"GT"},{MULTIPLICATION,"MULTIPLICATION"},{DOTPOWER,"DOTPOWER"},{END,"END"},{LE,"LE"},{STRING,"STRING"}};
map<int,string> keywords_naming_dictionary;


// a coordinate inside input text
typedef struct {
	int line;
	int column;
} t_text_coordinates;

string text = ""; // input string
int len;          // input string length
int text_idx = 0; // index to the current character in input string
int text_start_index = 0; // start of the current token to be processed
t_text_coordinates *curr_token_start_pos; // start position of the current token to be processed
int line;         // current line position of the lexer
int column;       // current column position of the lexer

// token information
typedef struct {
	int token;
	t_text_coordinates text_coordinates;
	string token_text;
	int string_start_index;
	int string_end_index;
} t_token_info;

// input token stream
list<t_token_info> tokens;

void init(){
	int i,n;
	// init keywords list
	if (keywords.empty() == false){
		keywords.clear();
	}
	n = sizeof(_keywords) / sizeof(int);
	for (i=0;i<n;i++){
		keywords.push_back(_keywords[i]);
	}
	// init keywords dictionary
	if (keywords_dictionary.empty() == false){
		keywords_dictionary.clear();
	}
	n = sizeof(_keywords_dictionary) / sizeof(t_keywords_dictionary);
	for(i=0;i<n;i++){
		t_keywords_dictionary t = _keywords_dictionary[i];
		keywords_dictionary[t.key] = t.value;
	}
	// init keywords naming dictionary
	if (keywords_naming_dictionary.empty() == false){
		keywords_naming_dictionary.clear();
	}
	n = sizeof(_keywords_naming_dictionary) / sizeof(t_keywords_naming_dictionary);
	for (i=0;i<n;i++){
		t_keywords_naming_dictionary t = _keywords_naming_dictionary[i];
		keywords_naming_dictionary[t.key] = t.value;
	}
	// clear token stream
	tokens.clear();
}

// current lexer position in text coordinates
t_text_coordinates *GetCurrentPosition(){
	t_text_coordinates *text_position = new t_text_coordinates();
	text_position->line = line;
	text_position->column = column;
	return text_position;
}

bool _containsKeyword(int keyword){
	int n = keywords.size();
	for(int i=0;i<n;i++){
		if (_keywords[i] == keyword){
			return true;
		}
	}
	return false;
}

bool _containsKeywordDictionary(string keyword, int &token){
	int n = sizeof(_keywords_dictionary) / sizeof(t_keywords_dictionary);
	for(int i=0;i<n;i++){
		if (_keywords_dictionary[i].key == keyword){
			token = _keywords_dictionary[i].value;
			return true;
		}
	}
	return false;
}

// add a token to the current token stream
//   where start_position .. start index in input text,
//         token_text .. string representation of token, it may be the value for specific token types
void _addToken(int type, int start_position, int stop_position, string token_text){
	t_token_info newToken;
	// infer from current lexer state
	newToken.text_coordinates = *curr_token_start_pos;

	newToken.token = type;
	newToken.string_start_index = start_position;
	newToken.string_end_index = stop_position;
	newToken.token_text = token_text;

	tokens.push_back(newToken);
}

// same as _addToken(), but default text is looked up in naming dictionary
void AddToken(int type, int start_position, int stop_position){
	string mytext = "";
	if (type == ID || type == COMPLEX || type == REAL || type == STRING || type == ERROR ||
			_containsKeyword(type)
	)
	{
		mytext = text.substr(text_start_index, text_idx - text_start_index);
	}
	else{
		mytext = keywords_naming_dictionary[type];
	}
	_addToken(type, start_position, stop_position, mytext);
}

void ReplaceToken(int type, int start_position, int stop_position){
	if (tokens.empty() == false){
		tokens.pop_back();
	}
	AddToken(type, start_position, stop_position);
}

int GetCharacterIndex(){
	return text_idx;
}

bool isDigit(char c){
	return (c >= '0' && c <= '9');
}
bool isAlpha(char c){
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}
bool isOperator(char c){
	return (c == '+' || c == '-' || c == '*' || c == '^' || c == '\'' || c == '\\' || c == '/');
}
bool isAlphaNumeric(char c){
	return isDigit(c) || isAlpha(c);
}
bool isUnparsedCharacter(char c){
	return (c != '\'');
}

// check whether the current and following characters make up identifiers
// if it matches then lexer is updated
bool MatchIdentifier(){
	if (isAlpha(text[text_idx]) == false){
		return false;
	}
	text_idx++; column++;
	while (text_idx < len && (isAlphaNumeric(text[text_idx]) || text[text_idx] == '_')){
		text_idx++; column++;
	}
	AddToken(ID, text_start_index, text_idx -1);
	return true;
}

bool MatchReal(){
	char c = text[text_idx];

	if (isDigit(c) == false && c != '.'){
		return false;
	}
	if (isDigit(c)) // DIGIT+ ( DOT DIGIT*  | ( DOT DIGIT*)? EXPONENT)?
	{
		while (text_idx < len && isDigit(text[text_idx]))  // DIGIT+
		{
			text_idx++; column++;
		}
		if (text_idx >= len){
			AddToken(REAL, text_start_index, text_idx - 1);
			return true;
		}
		c = text[text_idx];
		if (c == '.')  // DOT DIGIT* EXPONENT
		{
			// skip rare case: e.g. 3.^9 => (3).^9
			if ((text_idx + 1 < len) && isOperator(text[text_idx + 1]))
			{
				AddToken(REAL, text_start_index, text_idx - 1);
				return true;
			}
			text_idx++; column++;
			while (text_idx < len && isDigit(text[text_idx]))  // DIGIT*
			{
				text_idx++; column++;
			}
			if (text_idx >= len){
				AddToken(REAL, text_start_index, text_idx - 1);
				return true;
			}
			c = text[text_idx];
			if (c == 'e' || c == 'E') // EXPONENT: ('e'|'E') (PLUS|MINUS)? DIGIT+
			{
				text_idx++; column++;
				if (text_idx >= len){
					AddToken(ERROR, text_start_index, text_idx - 1); // I
					return true;
				}
				c = text[text_idx];
				if (c == '+' || c == '-')  // (PLUS|MINUS)?
				{
					text_idx++; column++;
				}
				c = text[text_idx]; // DIGIT+
				if (isDigit(c) == false){
					AddToken(ERROR, text_start_index, text_idx - 1); // II
					return true;
				}
				else{
					text_idx++; column++;
					while (text_idx < len && isDigit(text[text_idx])){
						text_idx++; column++;
					}
				}
			}
		}
		else if (c == 'e' || c == 'E') // EXPONENT: ('e'|'E') (PLUS|MINUS)? DIGIT+
		{
			c = text[++text_idx]; column++;
			if (c == '+' || c == '-'){ // (PLUS|MINUS)?
				text_idx++; column++;
			}
			c = text[text_idx];  // DIGIT+
			if (isDigit(c) == false){
				AddToken(ERROR, text_start_index, text_idx - 1); // III
				return true;
			}
			else{
				text_idx++; column++;
				while (text_idx < len && isDigit(text[text_idx])){
					text_idx++; column++;
				}
			}
		}
	}
	else if (c == '.') // DOT DIGIT+ EXPONENT?
	{
		c = text[++text_idx]; column++;  // DIGIT+
		if (isDigit(c) == false){
			text_idx++; column++;
			AddToken(ERROR, text_start_index, text_idx - 1); // VI
			return true;
		}
		else{
			text_idx++; column++;
			while (text_idx < len && isDigit(text[text_idx])){
				text_idx++; column++;
			}
		}
		if (text_idx >= len){
			AddToken(REAL, text_start_index, text_idx - 1);
			return true;
		}
		c = text[text_idx];
		if (c == 'e' || c == 'E') // EXPONENT: ('e'|'E') (PLUS|MINUS)? DIGIT+
		{
			text_idx++; column++;
			if (text_idx >= len){
				AddToken(ERROR, text_start_index, text_idx - 1); // IV
				return true;
			}
			c = text[text_idx];
			if (c == '+' || c == '-') // (PLUS|MINUS)?
			{
				text_idx++; column++;
				if (text_idx >= len){
					AddToken(ERROR, text_start_index, text_idx - 1); // V
					return true;
				}
			}
			c = text[text_idx];
			// DIGIT+
			if (isDigit(c) == false){
				AddToken(ERROR, text_start_index, text_idx - 1); // VI
				return true;
			}
			else{
				text_idx++; column++;
				while (text_idx < len && isDigit(text[text_idx])){
					text_idx++; column++;
				}
			}
		}
	}
	AddToken(REAL, text_start_index, text_idx - 1);
	return true;
}

bool MatchString(){
	char c = text[text_idx];
	bool nonEmpty = false;

	if (c != '\''){
		return false;
	}
	text_idx++; column++;
	while (text_idx < len && ((text[text_idx] == '\'' && (text_idx+1 < len) && text[text_idx+1] == '\'')
			                   || isUnparsedCharacter(text[text_idx]))) // (...)+
	{
		if (text[text_idx] == '\''){
			text_idx++; column++;
		}
		text_idx++; column++;
		nonEmpty = true;
	}
	if (text_idx >= len){
		if (nonEmpty == true){
			AddToken(ERROR, text_start_index, text_idx - 1);
			return true;
		}
		else{
			AddToken(TRANSPOSE, text_start_index, text_idx - 1);
			return true;
		}
	}
	else if (text[text_idx] != '\''){
		AddToken(ERROR, text_start_index, text_idx - 1);
		return true;
	}
	text_idx++; column++;
	AddToken(STRING, text_start_index, text_idx - 1);
	return true;
}

list<t_token_info> lex(){
	len = text.length();
	line = 0;
	column = 0;

	while (text_idx < len){
		bool simpleToken = true;
		char c = text[text_idx];

		// resync on every token start
		text_start_index = text_idx;
		curr_token_start_pos = GetCurrentPosition();

		switch (c)
		{
			// 1-character tokens
			case '+': AddToken(PLUS, text_start_index, text_start_index); break;
			case '-': AddToken(MINUS, text_start_index, text_start_index); break;
			case '*': AddToken(MULTIPLICATION, text_start_index, text_start_index); break;
			case '/': AddToken(RIGHTDIVISION, text_start_index, text_start_index); break;
			case '^': AddToken(POWER, text_start_index, text_start_index); break;
			case ':': AddToken(COLON, text_start_index, text_start_index); break;
			case ';': AddToken(SEMICOLON, text_start_index, text_start_index); break;
			case ',': AddToken(COMMA, text_start_index, text_start_index); break;
			case '(': AddToken(LBRACKET, text_start_index, text_start_index); break;
			case ')': AddToken(RBRACKET, text_start_index, text_start_index);
					  // treat all following "\'" as separate transposes
					  while (text_idx + 1 < len && text[text_idx + 1] == '\'')
					  {
						  text_idx++; column++;
						  // resync token position
						  text_start_index = text_idx;
						  curr_token_start_pos = GetCurrentPosition();
						  // add transpose as separate new token
						  AddToken(TRANSPOSE, text_idx, text_idx);
					  } break;
			case '[': AddToken(LSBRACKET, text_start_index, text_start_index); break;
			case ']': AddToken(RSBRACKET, text_start_index, text_start_index);
					  // treat all following "\'" as separate transpose tokens
					  while (text_idx+1 < len && text[text_idx + 1] == '\'')
					  {
						  text_idx++; column++;
						  // resync token position
						  text_start_index = text_idx;
						  curr_token_start_pos = GetCurrentPosition();
						  // add transpose as separate new token
						  AddToken(TRANSPOSE, text_idx, text_idx);
					  } break;
			case '\\': AddToken(LEFTDIVISION, text_start_index, text_start_index); break;
			case '\n': line++; column = -1; AddToken(LINEBREAK, text_start_index, text_start_index); break;
			// 2-and more-character tokens
			case '%': text_idx++; column++;
					  while (text_idx < len && text[text_idx] != '\n')
					  {
						  text_idx++; column++;
					  }
					  if (text_idx < len && text[text_idx] == '\n')
					  {
						  line++; column = -1;
					  }
					  text_idx--;
					  AddToken(WS, text_start_index, text_idx);
					  break;
			case '\t':
			case ' ': AddToken(WS, text_start_index, text_start_index);
					  ++text_idx; column++;
					  if (text_idx < len){
						  while ((text_idx < len) && (text[text_idx] == ' ' || text[text_idx] == '\t')){
							  // resync furhter white space token position
							  text_start_index = text_idx;
							  curr_token_start_pos = GetCurrentPosition();

							  AddToken(WS, text_idx, text_idx);
							  text_idx++; column++;
						  }
					  }
					  text_idx--; column--;
					  break;
			case '\r': if (text_idx < len - 1){
							c = text[text_idx + 1];
							switch (c){
								case '\n': ++text_idx; line++; column = -1; AddToken(LINEBREAK, text_start_index, text_start_index + 1); break;
								default: text_idx++; column++; // I)
										 AddToken(ERROR, text_start_index, text_start_index);
										 text_idx--; column--;
										 break;
							}
					   }
					   else{
						   text_idx++; column++; // II)
						   AddToken(ERROR, text_start_index, text_start_index);
						   text_idx--; column--;
					   }
					   break;
			case '<':  if (text_idx < len -1){
					   	   c = text[text_idx + 1];
					   	   switch (c){
					   	   	   case '=': ++text_idx; column++; AddToken(LE, text_start_index, text_start_index + 1); break;
					   	   	   default: AddToken(LT, text_start_index, text_start_index); break;
					   	   }
					   }
					   else{
						   AddToken(LT, text_start_index, text_start_index);
					   }
					   break;
			case '>':  if (text_idx < len - 1){
					   	   c = text[text_idx + 1];
					   	   switch (c){
					   	   	   case '=': ++text_idx; column++; AddToken(GE, text_start_index, text_start_index + 1); break;
					   	   	   default: AddToken(GT, text_start_index, text_start_index); break;
					   	   }
					   }
					   else{
						   AddToken(GT, text_start_index, text_start_index);
					   }
			           break;
			case '&': if (text_idx < len - 1){
					  	  c = text[text_idx+1];
					  	  switch (c){
					  	  	  case '&': ++text_idx; column++; AddToken(ANDAND, text_start_index, text_start_index + 1); break;
					  	  	  default: AddToken(AND, text_start_index, text_start_index); break;
					  	  }
					  }
					  else{
						  AddToken(AND, text_start_index, text_start_index);
					  }
					  break;
			case '|': if (text_idx < len - 1){
					  	  c = text[text_idx+1];
					  	  switch(c){
					  	  	  case '|': ++text_idx; column++; AddToken(OROR, text_start_index, text_start_index + 1); break;
					  	  	  default: AddToken(OR, text_start_index, text_start_index); break;
					  	  }
					  }
					  else{
						  AddToken(OR, text_start_index, text_start_index);
					  }
					  break;
			case '~': if (text_idx < len - 1){
					  	  c = text[text_idx + 1];
					  	  switch (c){
					  	  	  case '=': ++text_idx; column++;
					  	  	  	  	  	AddToken(NE, text_start_index, text_start_index + 1);
					  	  	  	  	  	break;
					  	  	  default: AddToken(NOT, text_start_index, text_start_index); break;
					  	  }
					  }
					  else{
						  AddToken(NOT, text_start_index, text_start_index);
					  }
					  break;
			case '=': if (text_idx < len - 1){
					  	  c = text[text_idx + 1];
					  	  switch (c){
					  	  	  case '=': ++text_idx; column++;
					  	  	  	  	  	AddToken(EQ, text_start_index, text_start_index + 1);
					  	  	  	  	  	break;
					  	  	  default: AddToken(EQUALS, text_start_index, text_start_index); break;
					  	  }
					  }
					  else{
						  AddToken(EQUALS, text_start_index, text_start_index);
					  }
					  break;
			case '.': if (text_idx < len - 1){
					  	  c = text[text_idx+1];
					  	  if (isDigit(c) == true){
					  		  simpleToken = false; // it is par of a real
					  		  break;
					  	  }
					  	  else if (c == '.' && (text_idx + 2 < len) && text[text_idx + 2] == '.'){ // THREE_DOTS
					  		  text_idx += 3; column += 3;
					  		  if (text_idx >= len){
					  			  AddToken(THREE_DOTS, text_start_index, text_start_index + 2);
					  			  text_idx--; column--;
					  			  break;
					  		  }
					  		  do{
					  			  text_idx++; column++;
					  		  } while(text_idx < len && text[text_idx] != '\n'); // ~'\n'*
					  		  if (text_idx >= len){
					  			  text_idx--;
					  		  }
					  		  else if (text[text_idx] == '\n'){
					  			  line++;
					  			  column = -1; // new line is reached
					  		  }
					  		  AddToken(THREE_DOTS, text_start_index, text_idx);
					  	  }
					  	  else{
					  		  switch (c){
					  		  	  case '*': ++text_idx; column++; AddToken(DOTMULTIPLICATION, text_start_index, text_start_index + 1); break;
					  		  	  case '^': ++text_idx; column++; AddToken(DOTPOWER, text_start_index, text_start_index + 1); break;
					  		  	  case '/': ++text_idx; column++; AddToken(DOTRIGHTDIVISION, text_start_index, text_start_index + 1); break;
					  		  	  case '\'': ++text_idx; column++; AddToken(DOTTRANSPOSE, text_start_index, text_start_index + 1); break;
					  		  	  case '\\': ++text_idx; column++; AddToken(DOTLEFTDIVISION, text_start_index, text_start_index + 1); break;
					  		  	  default: AddToken(DOT, text_start_index, text_start_index); break;
					  		  }
					  	  }
					  }
					  else{
					      AddToken(DOT, text_start_index, text_start_index);
					  }
					  break;
			default: simpleToken = false; break;
		}
		if (simpleToken == true){
			text_idx++; column++;
		}
		else{  // complex tokens
			int token_start = text_idx;
			if (MatchIdentifier()){
				// replace some identifiers if it was a keywords
				int token_end = text_idx;
				int token_length = token_end - token_start;
				string substring = text.substr(token_start, token_length);
				int new_token;
				if (_containsKeywordDictionary(substring, new_token)){
					// replace ID token by corresponding keyword token
					ReplaceToken(new_token, text_start_index, text_start_index + substring.length() - 1);
				}
				// treat all following "\'" as separate transpose tokens
				while (text_idx < len && text[text_idx] == '\''){
					text_start_index = text_idx; // resync token position
					curr_token_start_pos = GetCurrentPosition();
					AddToken(TRANSPOSE, text_idx, text_idx);
					text_idx++; column++;
				}
			}
			else if (text_idx < len && MatchReal()){
				if (text_idx >= len){
					break;
				}
				c = text[text_idx];
				// replace real if it was a complex
				if (c == 'i' || c == 'j'){
					text_idx++; column++;
					ReplaceToken(COMPLEX, text_start_index, text_idx - 1);
				}
				// treat all following "\'" as separate transpose
				while (text_idx < len && text[text_idx] == '\''){
					// resync transpose tokens are new tokens to be inserted
					text_start_index = text_idx;
					curr_token_start_pos = GetCurrentPosition();
					AddToken(TRANSPOSE, text_idx, text_idx);
					text_idx++; column++;
				}
			}
			else if (text_idx < len && MatchString()){
			}
			else {  // resync general error token
				text_start_index = text_idx;
				curr_token_start_pos = GetCurrentPosition();

				text_idx++; column++;
				AddToken(ERROR, text_start_index, text_start_index);
			}
		}
	} // elihw
	return tokens;
}

static int _getTokenWidth(t_token_info token, string text){
	string mytext = "";
	if (token.token == ID || token.token == COMPLEX || token.token == REAL || token.token == STRING || token.token == ERROR || _containsKeyword(token.token)){
		mytext = token.token_text;
	}
	else{
		mytext = text.substr(token.string_start_index, token.string_end_index - token.string_start_index + 1);
	}
	return mytext.length();
}

void _dumpTokens();

template<std::size_t N>
static void RunTest(string _text, const int (&mustB_tokens)[N]){
	// initialize all
	text = _text; text_idx = 0;
	init();

	unsigned int mustB_token_count  = N;
	list<t_token_info> _tokens = lex();
	_dumpTokens(); // ...
	assert(_tokens.size() == mustB_token_count);
	int i=0;
	std::list<t_token_info>::iterator it = _tokens.begin();
	for (; it != _tokens.end(); it++){
	    assert(it->token == mustB_tokens[i++]);
	}
	if (_tokens.empty() == false){
		it = _tokens.end();
		t_token_info last = *(--it);
		assert(last.string_end_index+1 == GetCharacterIndex());
	}
	// check consecutiveness of token indices + plausability of token width
	it = _tokens.begin();
	it++;
	t_token_info prev_tok, act_tok;
	std::list<t_token_info>::iterator it2 = _tokens.end();
	it2--;
	while (it != _tokens.end()) {
		if (it == it2){
			break;
		}
		prev_tok = *it;
		it++;
		act_tok = *it;

		// "no gaps between tokens"
		if(prev_tok.string_end_index+1 != act_tok.string_start_index){
			printf("FAILURE!");
		}
		assert(prev_tok.string_end_index+1 == act_tok.string_start_index);
		// START+WIDTH == END
		assert(prev_tok.string_start_index + _getTokenWidth(prev_tok, _text) -1 == prev_tok.string_end_index);
		assert(act_tok.string_start_index + _getTokenWidth(act_tok, _text)-1 == act_tok.string_end_index);
		// Text: 2 consecutive tokens must obey lexicographical text order
		bool lexico = prev_tok.text_coordinates.line < act_tok.text_coordinates.line ||
				      (prev_tok.text_coordinates.line == act_tok.text_coordinates.line && prev_tok.text_coordinates.column < act_tok.text_coordinates.column);
		assert(lexico);
		assert(prev_tok.text_coordinates.column >= 0 && act_tok.text_coordinates.column >= 0);
		assert(prev_tok.text_coordinates.line >= 0 && act_tok.text_coordinates.line >= 0);
	}
}

void _dumpTokens(){
	std::list<t_token_info>::iterator it;
	printf("TOKENS: [ ");
	for (it=tokens.begin(); it != tokens.end(); it++){
		if (it->token == ERROR){
			printf("ERROR (%d) ", it->token);
		}
		else{
			printf("%s (%d) ", keywords_naming_dictionary[it->token].c_str(), it->token);
		}
	}
	printf("]\n");
}

int main() {
	printf("Starting all tests\n");

	RunTest("\'a", (const int[]){ ERROR });
	RunTest("§'", (const int[]){ ERROR, ERROR, TRANSPOSE });
	RunTest("ä", (const int[]){ ERROR, ERROR });

	RunTest("a\ra", (const int[]){ ID, ERROR, ID });
	RunTest("end", (const int[]){ END });
	RunTest(".", (const int[]){ DOT });
	RunTest(".", (const int[]){ DOT });

	RunTest("function [x,y]=foo()\n x=5\n y=5\nend", (const int[]){ FUNCTION, WS, LSBRACKET, ID, COMMA, ID, RSBRACKET, EQUALS, ID, LBRACKET,RBRACKET,LINEBREAK,WS,ID,EQUALS, REAL, LINEBREAK, WS, ID, EQUALS, REAL, LINEBREAK, END});

	RunTest(".", (const int[]){ DOT });
	RunTest(".a", (const int[]){ DOT, ID });
	RunTest(".1z", (const int[]){ REAL, ID });

	// newlines
	RunTest("b\ra", (const int[]){ ID, ERROR, ID });
	RunTest("\r", (const int[]){ ERROR });
	RunTest("\ra", (const int[]){ ERROR, ID });

	// expected errors
	RunTest("abcdef+123% \nHH", (const int[]){ ID, PLUS, REAL, WS, LINEBREAK, ID });
	RunTest("34.1e", (const int[]){ ERROR });
	RunTest("1.e", (const int[]){ ERROR });
	RunTest(".123e-", (const int[]){ ERROR });
	RunTest("1.0e'", (const int[]){ ERROR, TRANSPOSE });
	RunTest("..1e", (const int[]){ DOT, ERROR });
	RunTest("@a", (const int[]){ ERROR, ID });
	RunTest("123eZ", (const int[]){ ERROR, ID });

	RunTest("'§", (const int[]){ ERROR }); //
	RunTest(".123eZ", (const int[]){ ERROR, ID });

	RunTest("1+ 1i1.e", (const int[]){ REAL, PLUS, WS, COMPLEX, ERROR });
	RunTest("Z1 123e(.5e.", (const int[]){ ID, WS, ERROR, LBRACKET, ERROR, DOT});
	RunTest("123eZ1(.5e", (const int[]){ ERROR, ID, LBRACKET, ERROR });
	RunTest("123eZ1.e", (const int[]){ ERROR, ID, DOT, ID });
	RunTest("123e Z", (const int[]){ ERROR, WS, ID });
	RunTest("1.e . .1", (const int[]){ ERROR, WS, DOT, WS, REAL });
	RunTest("1.e. .1", (const int[]){ ERROR, DOT, WS, REAL });
	RunTest("1.e..1", (const int[]){ ERROR, DOT, REAL });
	RunTest("1.ee", (const int[]){ ERROR, ID });
	RunTest("1.ee123", (const int[]){ ERROR, ID });
	RunTest("1.e 123ee", (const int[]){ ERROR, WS, ERROR, ID });
	RunTest("1.e 1.e", (const int[]){ ERROR, WS, ERROR });
	RunTest("1.e;1.e", (const int[]){ ERROR, SEMICOLON, ERROR });

	// arbitrary operators
	RunTest("&&&+&&", (const int[]){ ANDAND, AND, PLUS, ANDAND });
	RunTest(".*.^*.*", (const int[]){ DOTMULTIPLICATION, DOTPOWER, MULTIPLICATION, DOTMULTIPLICATION });
	RunTest("+-*.*", (const int[]){ PLUS, MINUS, MULTIPLICATION, DOTMULTIPLICATION });

	RunTest("~", (const int[]){ NOT });
	RunTest("~+=", (const int[]){ NOT, PLUS, EQUALS });
	RunTest("~=", (const int[]){ NE });
	RunTest("+~=+", (const int[]){ PLUS, NE, PLUS });
	RunTest("~=+", (const int[]){ NE, PLUS });
	RunTest("+~=", (const int[]){ PLUS, NE });
	RunTest("++~=", (const int[]){ PLUS, PLUS, NE });
	RunTest("~=++", (const int[]){ NE, PLUS, PLUS });
	RunTest("=", (const int[]){ EQUALS });
	RunTest("==", (const int[]){ EQ });
	RunTest("===", (const int[]){ EQ, EQUALS });
	RunTest("==+=*===", (const int[]){ EQ, PLUS, EQUALS, MULTIPLICATION, EQ, EQUALS });
	RunTest(">", (const int[]){ GT });
	RunTest(">=", (const int[]){ GE });
	RunTest(">==>", (const int[]){ GE, EQUALS, GT });
	RunTest(">==>=", (const int[]){ GE, EQUALS, GE });
	RunTest("<", (const int[]){ LT });
	RunTest("<=", (const int[]){ LE });
	RunTest("<=<=", (const int[]){ LE, LE });
	RunTest("<<=", (const int[]){ LT, LE });
	RunTest("=<=>", (const int[]){ EQUALS, LE, GT });
	RunTest("=<=>=", (const int[]){ EQUALS, LE, GE });
	RunTest("=<=>==", (const int[]){ EQUALS, LE, GE, EQUALS });

	RunTest("\\", (const int[]){ LEFTDIVISION });
	RunTest("\\.\\", (const int[]){ LEFTDIVISION, DOTLEFTDIVISION });
	RunTest(".\\+\\", (const int[]){ DOTLEFTDIVISION, PLUS, LEFTDIVISION });
	RunTest(".\\\\", (const int[]){ DOTLEFTDIVISION, LEFTDIVISION });

	// identifiers
	RunTest("a", (const int[]){ ID });
	RunTest("ab", (const int[]){ ID });
	RunTest("aab", (const int[]){ ID });
	RunTest("abc", (const int[]){ ID });
	RunTest("abc(", (const int[]){ ID, LBRACKET });
	RunTest(")abc", (const int[]){ RBRACKET, ID });
	RunTest(")abc(", (const int[]){ RBRACKET, ID, LBRACKET });

	// numbers
	RunTest(".1", (const int[]){ REAL });
	RunTest(".123", (const int[]){ REAL });
	RunTest(".1e2", (const int[]){ REAL });
	RunTest(".1e23", (const int[]){ REAL });
	RunTest(".12e23", (const int[]){ REAL });
	RunTest(".12e1", (const int[]){ REAL });
	RunTest(".1e+1", (const int[]){ REAL });
	RunTest(".1e+23", (const int[]){ REAL });
	RunTest(".1e-1", (const int[]){ REAL });
	RunTest(".1e-23", (const int[]){ REAL });
	RunTest(".122e1", (const int[]){ REAL });
	RunTest(".122e12", (const int[]){ REAL });
	RunTest(".123e+1", (const int[]){ REAL });
	RunTest(".123e+12", (const int[]){ REAL });
	RunTest(".123e-1", (const int[]){ REAL });
	RunTest(".123e-12", (const int[]){ REAL });

	RunTest("1", (const int[]){ REAL });
	RunTest("123", (const int[]){ REAL });
	RunTest("0123", (const int[]){ REAL });
	RunTest("10023", (const int[]){ REAL });
	RunTest("1.", (const int[]){ REAL });
	RunTest("1.0", (const int[]){ REAL });
	RunTest("1.00", (const int[]){ REAL });
	RunTest("1.0001", (const int[]){ REAL });
	RunTest("1.", (const int[]){ REAL });
	RunTest("1.e0", (const int[]){ REAL });
	RunTest("1.0e+15", (const int[]){ REAL });
	RunTest("1.00e-7", (const int[]){ REAL });
	RunTest("1.001e123", (const int[]){ REAL });
	RunTest("1.e1", (const int[]){ REAL });
	RunTest("1.0e12", (const int[]){ REAL });
	RunTest("1.00e+1", (const int[]){ REAL });
	RunTest("1.0001e+12", (const int[]){ REAL });
	RunTest("1.e-1", (const int[]){ REAL });
	RunTest("1.e-12", (const int[]){ REAL });
	RunTest("1.e+12", (const int[]){ REAL });

	RunTest("1e12", (const int[]){ REAL });
	RunTest("1e+12", (const int[]){ REAL });
	RunTest("1.e+12", (const int[]){ REAL });

	RunTest(".E-", (const int[]){ DOT, ID, MINUS });
	RunTest(".e", (const int[]){ DOT, ID });
	RunTest(".e123", (const int[]){ DOT, ID });
	RunTest(".e+123", (const int[]){ DOT, ID, PLUS, REAL });

	RunTest("..e", (const int[]){ DOT, DOT, ID });
	RunTest("..1", (const int[]){ DOT, REAL });
	RunTest("..1e+1", (const int[]){ DOT, REAL });

	RunTest("056989+", (const int[]){ REAL, PLUS });
	RunTest("+056989", (const int[]){ PLUS, REAL });
	RunTest("0+569*89+", (const int[]){ REAL, PLUS, REAL, MULTIPLICATION, REAL, PLUS });
	RunTest("1.e123e", (const int[]){ REAL, ID });

	// complex numbers
	RunTest("j", (const int[]){ ID });
	RunTest("1.e+12j", (const int[]){ COMPLEX });
	RunTest("1j", (const int[]){ COMPLEX });
	RunTest("1e+12j", (const int[]){ COMPLEX });
	RunTest("1.00e+1j", (const int[]){ COMPLEX });
	RunTest("1.00e+1i", (const int[]){ COMPLEX });
	RunTest("1.00e+1i+i", (const int[]){ COMPLEX, PLUS, ID });
	RunTest("1.00e+1i+1i+e", (const int[]){ COMPLEX, PLUS, COMPLEX, PLUS, ID });
	RunTest("1ij", (const int[]){ COMPLEX, ID });
	RunTest("1ji", (const int[]){ COMPLEX, ID });
	RunTest("1 i", (const int[]){ REAL, WS, ID });
	RunTest("1 +1i", (const int[]){ REAL, WS, PLUS, COMPLEX });
	RunTest("1+ 1i", (const int[]){ REAL, PLUS, WS, COMPLEX });
	RunTest("1e+12j", (const int[]){ COMPLEX });
	RunTest("123.456e78j", (const int[]){ COMPLEX });
	RunTest("123.456e+78i", (const int[]){ COMPLEX });

	RunTest("function", (const int[]){ FUNCTION });
	RunTest("functiona", (const int[]){ ID });
	RunTest("functio", (const int[]){ ID });
	RunTest("functionn", (const int[]){ ID });
	RunTest("afunction", (const int[]){ ID });
	RunTest("a1function", (const int[]){ ID });
	RunTest("a1+function", (const int[]){ ID, PLUS, FUNCTION });
	RunTest("function+", (const int[]){ FUNCTION, PLUS });
	RunTest("+function", (const int[]){ PLUS, FUNCTION });
	RunTest("-function", (const int[]){ MINUS, FUNCTION });
	RunTest("--function", (const int[]){ MINUS, MINUS, FUNCTION });
	RunTest("--function+-a", (const int[]){ MINUS, MINUS, FUNCTION, PLUS, MINUS, ID });
	RunTest("function+for+while", (const int[]){ FUNCTION, PLUS, FOR, PLUS, WHILE });
	RunTest("function+function", (const int[]){ FUNCTION, PLUS, FUNCTION });
	RunTest("+function-function", (const int[]){ PLUS, FUNCTION, MINUS, FUNCTION });
	RunTest("function+id+(+123.3+)", (const int[]){ FUNCTION, PLUS, ID, PLUS, LBRACKET, PLUS, REAL, PLUS, RBRACKET });

	RunTest("...", (const int[]){ THREE_DOTS });
	RunTest("...\n", (const int[]){ THREE_DOTS });
	RunTest("...abc\n", (const int[]){ THREE_DOTS });
	RunTest("... abc \n", (const int[]){ THREE_DOTS });
	RunTest("... abc (767\t tets3+3 \n)", (const int[]){ THREE_DOTS, RBRACKET });
	RunTest("... abc (767\t tets3+3 \nabc", (const int[]){ THREE_DOTS, ID });
	RunTest("... abc \nab", (const int[]){ THREE_DOTS, ID });
	RunTest("... abc \nab... (+45))", (const int[]){ THREE_DOTS, ID, THREE_DOTS });

	RunTest("\n", (const int[]){ LINEBREAK });
	RunTest("\r\n", (const int[]){ LINEBREAK });
	RunTest("abc\ncd", (const int[]){ ID, LINEBREAK, ID});

	RunTest("\'\'", (const int[]){ STRING });
	RunTest("\'abcdef\'", (const int[]){ STRING });
	RunTest("\'\'\'\'", (const int[]){ STRING });
	RunTest("\'ab\'\'\'", (const int[]){ STRING });
	RunTest("\'\'\'ab\'", (const int[]){ STRING });

	RunTest(" ", (const int[]){ WS });
	RunTest("1+ 2", (const int[]){ REAL, PLUS, WS, REAL });
	RunTest("1+  2", (const int[]){ REAL, PLUS, WS, WS, REAL});
	RunTest(" 1 ", (const int[]){ WS, REAL, WS });
	RunTest(" 1.3 ) ", (const int[]){ WS, REAL, WS, RBRACKET, WS });

	RunTest("%", (const int[]){ WS });
	RunTest("% ", (const int[]){ WS });
	RunTest("% \t", (const int[]){ WS });
	RunTest("% \t ", (const int[]){ WS });
	RunTest("% \n", (const int[]){ WS, LINEBREAK });
	RunTest("% \n ", (const int[]){ WS, LINEBREAK, WS });
	RunTest("% \n\n", (const int[]){ WS, LINEBREAK, LINEBREAK });
	RunTest("% \n%abcdefgh", (const int[]){ WS, LINEBREAK, WS });
	RunTest("% \n%abcdefgh", (const int[]){ WS, LINEBREAK, WS });
	RunTest("% \n%abcdefgh\n ", (const int[]){ WS, LINEBREAK, WS, LINEBREAK, WS });

	// strings
	RunTest("\'abcdef\'", (const int[]){ STRING });
	RunTest("(\'\'(\'\'", (const int[]){ LBRACKET, STRING, LBRACKET, STRING });
	RunTest("(\'\'\'\'", (const int[]){ LBRACKET, STRING });
	RunTest("\'\'(\'\'ab", (const int[]){ STRING, LBRACKET, STRING, ID });
	RunTest("\'\'\'\'ab", (const int[]){ STRING, ID });
	RunTest("\'ab\'(\'\'uu", (const int[]){ STRING, LBRACKET, STRING, ID });
	RunTest("\'ab\'\'\'uu", (const int[]){ STRING, ID });
	RunTest("\'\'(\'ab\'uu", (const int[]){ STRING, LBRACKET, STRING, ID });
	RunTest("\'\'\'ab\'uu", (const int[]){ STRING, ID});

	// rare REAL-cases
	RunTest("3.^9", (const int[]){ REAL, DOTPOWER, REAL });
	RunTest("1234.^9", (const int[]){ REAL, DOTPOWER, REAL });
	RunTest("3.^789", (const int[]){ REAL, DOTPOWER, REAL });

	// transposes
	RunTest("\'", (const int[]){ TRANSPOSE });
	RunTest("a'", (const int[]){ ID, TRANSPOSE });
	RunTest("a\'", (const int[]){ ID, TRANSPOSE });
	RunTest("\'abcd\'ffff\'", (const int[]){ STRING, ID, TRANSPOSE });
	RunTest(")'", (const int[]){ RBRACKET, TRANSPOSE });
	RunTest("]'", (const int[]){ RSBRACKET, TRANSPOSE });
	RunTest("1.3'", (const int[]){ REAL, TRANSPOSE });
	RunTest("1.3j'", (const int[]){ COMPLEX, TRANSPOSE });
	RunTest(".'", (const int[]){ DOTTRANSPOSE });
	RunTest(".''", (const int[]){ DOTTRANSPOSE, TRANSPOSE });

	RunTest("a''", (const int[]){ ID, TRANSPOSE, TRANSPOSE });
	RunTest("a'''", (const int[]){ ID, TRANSPOSE, TRANSPOSE, TRANSPOSE });
	RunTest("a'''b'", (const int[]){ ID, TRANSPOSE, TRANSPOSE, TRANSPOSE, ID, TRANSPOSE });
	RunTest("a''1", (const int[]){ ID, TRANSPOSE, TRANSPOSE, REAL });
	RunTest("a'''b", (const int[]){ ID, TRANSPOSE, TRANSPOSE, TRANSPOSE, ID });

	RunTest("1'", (const int[]){ REAL, TRANSPOSE });
	RunTest("1.'", (const int[]){ REAL, DOTTRANSPOSE });
	RunTest("1.0'", (const int[]){ REAL, TRANSPOSE });
	RunTest("1.2e3'", (const int[]){ REAL, TRANSPOSE });
	RunTest("1.23e4'", (const int[]){ REAL, TRANSPOSE });
	RunTest("1.23e+4'", (const int[]){ REAL, TRANSPOSE });
	RunTest("1.23e-4'", (const int[]){ REAL, TRANSPOSE });
	RunTest(")'", (const int[]){ RBRACKET, TRANSPOSE });
	RunTest("))'", (const int[]){ RBRACKET, RBRACKET, TRANSPOSE });
	RunTest(")) '", (const int[]){ RBRACKET, RBRACKET, WS, TRANSPOSE });
	RunTest(")\'\'", (const int[]){ RBRACKET, TRANSPOSE, TRANSPOSE });

	RunTest("]'", (const int[]){ RSBRACKET, TRANSPOSE });
	RunTest(")]'", (const int[]){ RBRACKET, RSBRACKET, TRANSPOSE });
	RunTest("]\'\'", (const int[]){ RSBRACKET, TRANSPOSE, TRANSPOSE });

	RunTest("\'\'ab\'\'", (const int[]){ STRING, ID, TRANSPOSE, TRANSPOSE });
	RunTest("ab\'\'(\'\'", (const int[]){ ID, TRANSPOSE, TRANSPOSE, LBRACKET, STRING });
	RunTest("ab\'\'\'\'", (const int[]){ ID, TRANSPOSE, TRANSPOSE, TRANSPOSE, TRANSPOSE });
	RunTest("a\'\'(\'\'ab", (const int[]){ ID, TRANSPOSE, TRANSPOSE, LBRACKET, STRING, ID });
	RunTest("\'a\'bc\'\'d\'", (const int[]){ STRING, ID, TRANSPOSE, TRANSPOSE, ID, TRANSPOSE });
	RunTest("(1+1)\'a\'bc\'\'d\'", (const int[]){ LBRACKET, REAL, PLUS, REAL, RBRACKET, TRANSPOSE, ID, TRANSPOSE, ID, TRANSPOSE, TRANSPOSE, ID, TRANSPOSE });

	// individual tests
	RunTest("5.//", (const int[]){ REAL, DOTRIGHTDIVISION, RIGHTDIVISION });
	RunTest("..5e5", (const int[]){ DOT, REAL });
	RunTest("5./6", (const int[]){ REAL, DOTRIGHTDIVISION, REAL });
	RunTest(".e5", (const int[]){ DOT, ID });
	RunTest("..e5", (const int[]){ DOT, DOT, ID });
	RunTest("34.e+1+5e3+e -15.6", (const int[]){ REAL, PLUS, REAL, PLUS, ID, WS, MINUS, REAL });

	RunTest("345.435", (const int[]){ REAL });
	RunTest("\t345.435", (const int[]){ WS, REAL });
	RunTest("345.", (const int[]){ REAL });
	RunTest("12.", (const int[]){ REAL });
	RunTest(".1e+0", (const int[]){ REAL });
	RunTest("1.e+2", (const int[]){ REAL });
	RunTest("1.e-0", (const int[]){ REAL });

	RunTest("34.e+1", (const int[]){ REAL });
	RunTest("34.1e+0", (const int[]){ REAL });
	RunTest(".34e-0", (const int[]){ REAL });
	RunTest("34.e-0", (const int[]){ REAL });

	RunTest("qed", (const int[]){ ID });
	RunTest("qed % comment \n% further comment", (const int[]){ ID, WS, WS, LINEBREAK, WS });
	RunTest("Super2211", (const int[]){ ID });
	RunTest("a", (const int[]){ ID });
	RunTest("a_a", (const int[]){ ID });
	RunTest("145345", (const int[]){ REAL });
	RunTest("123abc", (const int[]){ REAL, ID });
	RunTest(";", (const int[]){ SEMICOLON });
	RunTest(".*", (const int[]){ DOTMULTIPLICATION });
	RunTest("./", (const int[]){ DOTRIGHTDIVISION });

	RunTest("/", (const int[]){ RIGHTDIVISION });
	RunTest("\\", (const int[]){ LEFTDIVISION });
	RunTest("^", (const int[]){ POWER });
	RunTest(".^", (const int[]){ DOTPOWER });
	RunTest(".\\", (const int[]){ DOTLEFTDIVISION });
	RunTest("'", (const int[]){ TRANSPOSE });
	RunTest(".'", (const int[]){ DOTTRANSPOSE });
	RunTest("(", (const int[]){ LBRACKET });
	RunTest(")", (const int[]){ RBRACKET });
	RunTest("42i", (const int[]){ COMPLEX });
	RunTest("42j", (const int[]){ COMPLEX });
	RunTest("34e+1", (const int[]){ REAL });
	RunTest("34.e+1", (const int[]){ REAL });
	RunTest("3e-1", (const int[]){ REAL });
	RunTest("5e3", (const int[]){ REAL });
	RunTest(".5e3", (const int[]){ REAL });
	RunTest("||", (const int[]){ OROR });
	RunTest("&&", (const int[]){ ANDAND });
	RunTest("''", (const int[]){ STRING });
	RunTest("'ä'", (const int[]){ STRING });
	RunTest("'§'", (const int[]){ STRING });
	RunTest("'!#$%&()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_`abcdefghiklmnopqrstuvwxyz{|}~'", (const int[]){ STRING });
	RunTest("'\t'", (const int[]){ STRING });
	RunTest("''''", (const int[]){ STRING });
	RunTest("'a''a'", (const int[]){ STRING });
	RunTest("''''''", (const int[]){ STRING });
	RunTest("'abc''def'", (const int[]){ STRING });

	RunTest("switch", (const int[]){ SWITCH });
	RunTest("case", (const int[]){ CASE });
	RunTest("otherwise", (const int[]){ OTHERWISE });
	RunTest("for", (const int[]){ FOR });
	RunTest("function", (const int[]){ FUNCTION });

	printf("Run all tests successfully! \n\n");
	return 0;
}
