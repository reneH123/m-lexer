Name        : MatlabLexer.cpp  (applicable to MatLab / GNU Octave)

Author      : Rene Haberland

License     : CC BY-NC  https://creativecommons.org/licenses/by-nd/4.0/

Description : One-Pass Lexer for a multi-dimensional lexical analysis

Date        : 2016/2020


g++ MatlabLexer.cpp -o mlexer
